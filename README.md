# Usage

Go to _Settings – CI/CD – General pipelines – CI/CD configuration file_ of your project, and paste one of the following links

- `cube-ide-1.7.0/.gitlab-ci.yml@nxtlabs/shared-tools/cube-docker-images`
- `cube-ide-1.6.1/.gitlab-ci.yml@nxtlabs/shared-tools/cube-docker-images`

# Source of packages

- Manually downloaded files called _STM32CubeIDE Debian Linux Installer_ from https://www.st.com/en/development-tools/stm32cubeide.html  

## 1.7.0

```shell
unzip en.st-stm32cubeide_1.7.0_10852_20210715_0634_amd64.deb_bundle.sh_v1.7.0.zip
bash st-stm32cubeide_1.7.0_10852_20210715_0634_amd64.deb_bundle.sh --noexec --target .
```

It yielded:

- `st-stlink-udev-rules-1.0.3-2-linux-all.deb`
- `st-stlink-server-2.0.2-3-linux-amd64.deb`
- `st-stm32cubeide-1.7.0-10852-20210715-0634_amd64.deb`

## 1.6.1

```shell
unzip en.st-stm32cubeide_1.6.1_9958_20210326_1446_amd64.deb_bundle.sh.zip
bash st-stm32cubeide_1.6.1_9958_20210326_1446_amd64.deb_bundle.sh --noexec --target .
```

It yielded:

- `st-stlink-udev-rules-1.0.2-4-linux-all.deb`
- `st-stlink-server-2.0.2-1-linux-amd64.deb`
- `st-stm32cubeide-1.6.1-9958-20210326-1446_amd64.deb`
